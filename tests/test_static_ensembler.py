import unittest

from d3m import container
from pandas.testing import assert_frame_equal

from sri.autoflow import static_ensembler as se
from tests import util

class TestStaticEnsembler(unittest.TestCase):
    def test_produce(self):
        hyperparams = dict(se.StaticEnsemblerHyperparams.defaults())

        # Suppose three predictors over 4 classes
        hyperparams['weights'] = [0.8, 0.2, 0.5]
        hyperparams['class_count'] = 4

        ensembler = se.StaticEnsembler(hyperparams=hyperparams)

        data = container.DataFrame([
            [1, 1, 3],
            [2, 2, 2],
            [3, 4, 3],
            [3, 1, 1],
            [2, 3, 4]
        ])

        expected = container.DataFrame([
            [1],
            [2],
            [3],
            [3],
            [2]
        ])

        result = ensembler.produce(inputs=data)
        assert_frame_equal(result.value, expected)
